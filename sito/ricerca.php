<!DOCTYPE html>
<html>
    <?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

if(session_status()==PHP_SESSION_NONE){
  session_start();
}

require_once("addNotifica.php");
require_once("registrazione.php");
require_once("accesso.php");
require_once("logout.php");
require_once("addToCart.php");
?>
<head>
    <meta charset="utf-8">
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://kit.fontawesome.com/60e43df7e9.js" crossorigin="anonymous"></script>
    <link href="./style.css" rel="stylesheet" type="text/css">
</head>

<body>
    <div id="mobile-navbar" class="slider show">
        <button><i class="fas fa-times" onclick="slideOut('mobile-navbar')"></i></button>
    </div>
    <div id="filters" class="slider show">
        <button><i class="fas fa-times" onclick="slideOut('filters')"></i></button>
        <h2> Categorie </h2>
                <ul>
                    <?php
                        $conn = new mysqli('localhost','root','','ecommerce');
                        $query = $conn->query("SELECT * FROM categoria");
                        if($query ->num_rows){
                            while ($row = $query->fetch_assoc()){
                                $id_cat = $row["id"];
                                $nome_cat = $row["nome"];
                                echo("
                                <li>
                                <div class=\"dropdown\">
                                    <span class=\"dropdown-click\"><form style=\"display:inline-block;\" action=\"\" method=\"post\"><button type=\"submit\" name=\"filterCat\" style=\"background: white;color: black;font-size: 16px;padding: 0;height: auto;\">$nome_cat</button><input type=\"hidden\" value=\"$id_cat\" name=\"cat\"></form><i class=\"fas fa-chevron-down\"
                                            style=\"float: right;\"></i></span>
                                ");
                                $query_sotcat = $conn->query("SELECT * FROM sottocategoria WHERE id_cat = $id_cat");
                                if($query_sotcat ->num_rows){
                                    while ($row = $query_sotcat->fetch_assoc()){
                                        $id_sotcat = $row["id"];
                                        $nome_sotcat = $row["nome"];
                                        echo("
                                        <div class=\"dropdown-content\">
                                        <ul class=\"noborder\">
                                            <li><form action=\"\" method=\"post\"><button type=\"submit\" name=\"filterSotCat\" style=\"background: white;color: black;font-size: 16px;padding: 0;height: auto;\">$nome_sotcat</button><input type=\"hidden\" value=\"$id_sotcat\" name=\"sotcat\"></form></li>
                                        </ul>
                                    </div>
                                        ");
                                    }
                                }
                                echo("
                                </div>
                                </li> 
                                ");

                            }
                        }
                        $conn -> close();   
                    ?>
                    <!--<li>
                        <div class="dropdown">
                            <span class="dropdown-click">Dropdown 1<i class="fas fa-chevron-down"
                                    style="float: right;"></i></span>
                            <div class="dropdown-content">
                                <ul class="noborder">
                                    <li>Dongle 1</li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="dropdown">
                            <span class="dropdown-click">Dropdown 2<i class="fas fa-chevron-down"
                                    style="float: right;"></i></span>
                            <div class="dropdown-content">
                                <ul class="noborder">
                                    <li>Dongle 2</li>
                                </ul>
                            </div>
                        </div>
                    </li>-->
                </ul>
            </div>
    </div>
    <div class="modal-container flexbox align-center justify-center" id="login-container">
        <div class="modal fade" id="accedi">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="close"
                            onclick="toggle('login-container')"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Accedi a Cristocaves</h4>
                    </div>
                    <div id="errorMessage"></div>
                    <div class="modal-body">
                        <form action="" method="post" onSubmit="return validateUser();">
                            <input type="email" placeholder="Email" class="bordered form-control" name="emailAcc" id="emailAccedi">
                            <input type="password" placeholder="Password" class="bordered form-control" name="passAcc" id="passwordAccedi">
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Accedi</button>
                            </div>                        
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="modal-container flexbox align-center justify-center" id="register-container">
        <div class="modal fade" id="accedi">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="close"
                            onclick="toggle('register-container')"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Registrati a Cristocaves</h4>
                    </div>
                    <div id="errorMessage2"></div>
                    <div class="modal-body">
                        <form action="" method="post">
                            <input type="text" placeholder="Nome" class="bordered" class="form-control" name="namereg" id="nomeregCli">
                            <input type="email" placeholder="Email" class="bordered" class="form-control" name="emailreg" id="emailregCli">
                            <input type="password" placeholder="Password" class="bordered" class="form-control" name="passwordreg" id="passwordregCli">
                            <input type="password" placeholder="Conferma password" class="bordered" class="form-control"  id="confPasswordregCli">
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Registrati</button>
                            </div>
                        </form>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <header id="header">
            <div class="container flexbox justify-between align-center wrap" id="main-h">
                <h1 class="logo"><a href="index.php" style="color: white;text-decoration: none;">Plantemall</a></h1>
                <div>
                    <div class="controls flexbox align-center wrap">
                        <form action="ricerca.php" method="GET">
                            <div class="row">
                                <i class="fas fa-search"></i>
                                <input type="search" name="q" placeholder="Cerca una piantina...">
                            </div>
                        </form>
                        <div class="icon">
                            <a href="cart.php" style="color:white;"><i class="fas fa-shopping-cart"></i></a>
                        </div>
                        <div class="icon">
                            <a href="ordini.php" style="color:white;"><i class="fas fa-user"></i></a>
                        </div>
                    </div>
                    <div class="flexbox" style="margin-top: 10px;">
                        <?php
                            if(empty($_SESSION["email"])){
                        ?>
                        <button style="border: 2px solid white;" onclick="toggle('login-container')"> Login </button>
                        <button style="border: 2px solid white; margin-left: 10px;"
                            onclick="toggle('register-container')"> Registrati </button>
                        <?php
                            }else{
                        ?>
                            <form action="" method="post">
                                <button name="logout" style="border: 2px solid white; margin-left: 10px;">Logout da <?php echo($_SESSION["email"]); ?></button>
                            </form>
                        <?php
                            }
                        ?>
                    </div>
                </div>
            </div>
        <div id="navbar" class="flexbox align-center">
            <nav>
                <a href="index.php">Home</a>
                <a href="log.php">Notifiche</a>
                <a href="ordini.php">Ordini</a>
                <?php 
                    if(!empty($_SESSION["admin"])){
                ?>
                <a>|</a>
                <a href="dashboard-admin.php">Aggiungi prodotti</a>
                <a href="log_ordini.php">Log ordini</a>
                <a href="utenti.php">Utenti</a>
                <?php
                    }
                ?>
            </nav>
        </div>
    </header>
    <div id="main">
        <h1 id="main-title"> Risultati per: <?php echo($_GET["q"]) ?></h1>
        <button class="show" style="margin: 0 auto; margin-bottom: 40px;" onclick="slideIn('filters')">Filtri</button>
        <div class="flexbox align-top" id="main-grid">
            <div class="sidebar" id="main-sidebar">
                <h2> Categorie </h2>
                <ul>
                <?php
                        $conn = new mysqli('localhost','root','','ecommerce');
                        $query = $conn->query("SELECT * FROM categoria");
                        if($query ->num_rows){
                            while ($row = $query->fetch_assoc()){
                                $id_cat = $row["id"];
                                $nome_cat = $row["nome"];
                                echo("
                                <li>
                                <div class=\"dropdown\">
                                    <span class=\"dropdown-click\"><form style=\"display:inline-block;\" action=\"\" method=\"post\"><button type=\"submit\" name=\"filterCat\" style=\"background: white;color: black;font-size: 16px;padding: 0;height: auto;\">$nome_cat</button><input type=\"hidden\" value=\"$id_cat\" name=\"cat\"></form><i class=\"fas fa-chevron-down\"
                                            style=\"float: right;\"></i></span>
                                ");
                                $query_sotcat = $conn->query("SELECT * FROM sottocategoria WHERE id_cat = $id_cat");
                                if($query_sotcat ->num_rows){
                                    while ($row = $query_sotcat->fetch_assoc()){
                                        $id_sotcat = $row["id"];
                                        $nome_sotcat = $row["nome"];
                                        echo("
                                        <div class=\"dropdown-content\">
                                        <ul class=\"noborder\">
                                            <li><form action=\"\" method=\"post\"><button type=\"submit\" name=\"filterSotCat\" style=\"background: white;color: black;font-size: 16px;padding: 0;height: auto;\">$nome_sotcat</button><input type=\"hidden\" value=\"$id_sotcat\" name=\"sotcat\"></form></li>
                                        </ul>
                                    </div>
                                        ");
                                    }
                                }
                                echo("
                                </div>
                                </li> 
                                ");

                            }
                        }
                        $conn -> close();   
                    ?>
                    <!--<li>
                        <div class="dropdown">
                            <span class="dropdown-click">Dropdown 1<i class="fas fa-chevron-down"
                                    style="float: right;"></i></span>
                            <div class="dropdown-content">
                                <ul class="noborder">
                                    <li>Dongle 1</li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="dropdown">
                            <span class="dropdown-click">Dropdown 2<i class="fas fa-chevron-down"
                                    style="float: right;"></i></span>
                            <div class="dropdown-content">
                                <ul class="noborder">
                                    <li>Dongle 2</li>
                                </ul>
                            </div>
                        </div>
                    </li>-->
                </ul>
            </div>
            <div class="main-view flexbox wrap" id="main-flexbox">
                <?php
                    if(isset($_GET["q"])){
                      $conn = new mysqli('localhost','root','','ecommerce');
                      $ricerca = strtolower($_GET["q"]);
                      if(isset($_POST["filterCat"])){
                        $id_cat_filter = $_POST["cat"];
                        $query = $conn->query("SELECT * FROM prodotto WHERE lower(nome) LIKE '%$ricerca%' AND id_cat = $id_cat_filter ORDER BY nome");
                    }else if(isset($_POST["filterSotCat"])){
                        $id_sotcat_filter = $_POST["sotcat"];
                        $query = $conn->query("SELECT * FROM prodotto WHERE lower(nome) LIKE '%$ricerca%' AND id_sottocat = $id_sotcat_filter ORDER BY nome");
                    }else{
                        $query = $conn->query("SELECT * FROM prodotto WHERE lower(nome) LIKE '%$ricerca%' ORDER BY nome");
                    }
                      if($query ->num_rows){
                          while ($row = $query->fetch_assoc()){
                              $id_prod = $row["id"];
                              $nome = $row["nome"];
                              $descr = $row["descrizione"];
                              $img = $row["immagine"] . ".jpg";
                              $prezzo = $row["prezzo"];
                              $filename = $row["filename"];
                              echo("
                              <div class=\"item\">
                                  <a href=\"$filename\">
                                      <img src=\"images/$img\">
                                  </a>
                                  <p>$nome</p>
                                  <h2>$prezzo €</h2>
                                  <form method=\"post\">
                                      <button type=\"submit\" name=\"addToCart\"> Aggiungi al carrello </button>
                                      <input type=\"hidden\" value=\"1\" name=\"qnt\">
                                      <input type=\"hidden\" value=\"$id_prod\" name=\"id_prod\">
                                  </form>
                              </div>
                              ");
                          }
                      }
                    }
                ?>
            </div>
        </div>
    </div>
    <footer class ="footer">
        <div class ="footer-left"> 
            <h1>Plantemall</h1>
            <p>Siamo specializzati in vendita di svariati tipi di piante, il sito è stato creato per rendere possibile, anche durante la 
                pandemia di poter comprare le tue piante preferite comodamente da casa! La spedizione effettuata sarà sempre gratuita
                ed impiegherà al massimo 3 giorni per arrivare a casa tua, in modo da preservare al meglio l'integrità della pianta.
            </p>
            <div class ="socials">
                <a href="#"><i class="fa fa-facebook"></i></a>
                <a href="#"><i class="fa fa-twitter"></i></a>
                <a href="#"><i class="fa fa-instagram"></i></a>
            </div>
        </div>
        <ul class="footer-right">
            <li>
                <h1>Address</h1>
                <ul class ="box">
                    <li>Via dei Giardini, 12</li>
                    <li>Bassano del Grappa(VI)</li>
                    <li>Italia</li>
                </ul>
            </li>
            <li>
                <h1>Orari di apertura</h1>
                <ul class ="box">
                    <li>Martedì: 8.30-12.30 14.30-18.30</li>
                    <li>Mercoledì: 8.30-12.30 14.30-18.30</li>
                    <li>Giovedì: 8.30-12.30 14.30-18.30</li>
                    <li>Venerdì: 8.30-12.30 14.30-18.30</li>
                    <li>Sabato: 8.30-12.30 14.30-18.30</li>
                    <li>Domenica: 8.30-12.30 14.30-18.30</li>
                </ul>
            </li>
        </ul>
        <div class ="footer-bottom">
            <p>
                All Right reserved by davide.manieri@studio.unibo.it
            </p>
        </div>
    </footer>
</body>

</html>
<script>
    let dropdown_parent = document.getElementsByClassName('dropdown');
    Array.from(dropdown_parent)
        .forEach(item => {

            let children = Array.from(item.children);
            let event = Array.from(children[0].children);
            console.log(event[1]);

            event[1].addEventListener('click', (e) => {

                let icon = Array.from(children[0].children)[1]

                if (children[1].classList.contains('dropped')) {
                    
                    icon.style.transform = 'rotate(0)'
                }
                else {
                    icon.style.transform = 'rotate(180deg)'
                }

                for(var i=1; i<children.length; i++){
                    if (children[i].classList.contains('dropped')) {
                        children[i].style.display = 'none';
                        children[i].classList.remove('dropped');
                    }else{
                        children[i].style.display = 'block';
                        children[i].classList.add('dropped');
                    }
                }

            })

        })

</script>

<script>
    function registrationUser(){
      var nomeregC=document.getElementById("nomeregCli");
      var emailregC=document.getElementById("emailregCli");
      var pswregC=document.getElementById("passwordregCli");
      var confPswregC=document.getElementById("confPasswordregCli");
      var errorBox=document.getElementById("errorMessage2");
      var alertDiv='<div class="alert alert-danger alert-dismissible" role="alert">';
      var goodAlertDiv='<div class="alert alert-success alert-dismissible" role="alert">'
      var alertBtn='<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
    
      emailregC.style.border = "1px solid #ccc";
      pswregC.style.border = "1px solid #ccc";
      confEmailregC.style.border = "1px solid #ccc";
      confPswregC.style.border = "1px solid #ccc";
      nomeregC.style.border = "1px solid #ccc";
      cognomeregC.style.border = "1px solid #ccc";
    
      if(nomeregC.value==""){
        errorBox.innerHTML=alertDiv+alertBtn+'<strong>Attento</strong> hai dimenticato il nome!'+"</div>";
        nomeregC.focus();
        nomeregC.style.border = "3px solid #990033";
        return false;
      }
      if(emailregC.value==""){
        errorBox.innerHTML=alertDiv+alertBtn+'<strong>Attento</strong> hai dimenticato la mail!'+"</div>";
        emailregC.focus();
        emailregC.style.border = "3px solid #990033";
        return false;
      }
      if(pswregC.value==""){
        errorBox.innerHTML=alertDiv+alertBtn+'<strong>Attento</strong> hai dimenticato la password!'+"</div>";
        pswregC.focus();
        pswregC.style.border = "3px solid #990033";
        return false;
      }
      if(confPswregC.value==""){
        errorBox.innerHTML=alertDiv+alertBtn+'<strong>Attento</strong> hai dimenticato la conferma password!'+"</div>";
        confPswregC.focus();
        confPswregC.style.border = "3px solid #990033";
        return false;
      }
      if(pswregC.value!=confPswregC.value){
        errorBox.innerHTML=alertDiv+alertBtn+'<strong>Attento</strong> il campo conferma password non corrisponde!'+"</div>";
        confPswregC.value="";
        confPswregC.focus();
        confPswregC.style.border = "3px solid #990033";
        return false;
      }
    
      var reg=/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
      if(!reg.test(emailregC.value)){
        errorBox.innerHTML=alertDiv+alertBtn+'<strong>Attento</strong> il campo mail non è valido!'+"</div>";
        emailregC.focus();
        emailregC.style.border = "3px solid #990033";
        return false;
      }
      return true;
    }
    </script>

<script>
    function validateUser(){
        var emailCli=document.getElementById("emailAccedi");
        var pswCli=document.getElementById("passwordAccedi");
        var errorBox=document.getElementById("errorMessage");
        var alertDiv='<div class="alert alert-danger alert-dismissible" role="alert">';
        var alertBtn='<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
    
        emailCli.style.border = "1px solid #ccc";
        pswCli.style.border = "1px solid #ccc";
    
        if(emailCli.value==""){
        errorBox.innerHTML=alertDiv+alertBtn+'<strong>Attento</strong> hai dimenticato la mail!'+"</div>";
        emailCli.focus();
        emailCli.style.border = "3px solid #990033";
        return false;
        }
        if(pswCli.value==""){
        errorBox.innerHTML=alertDiv+alertBtn+'<strong>Attento</strong> hai dimenticato la password!'+"</div>";
        pswCli.focus();
        pswCli.style.border = "3px solid #990033";
        return false;
        }
        var reg=/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        if(!reg.test(emailCli.value)){
        errorBox.innerHTML=alertDiv+alertBtn+'<strong>Attento</strong> il campo mail non è valido!'+"</div>";
        emailCli.focus();
        emailCli.style.border = "3px solid #990033";
        return false;
        }
        return true;
    }
  </script>

<script src="./actions.js"></script>