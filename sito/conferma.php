<!DOCTYPE html>
<html>
<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

if(session_status()==PHP_SESSION_NONE){
  session_start();
}

require_once("addNotifica.php");
require_once("registrazione.php");
require_once("accesso.php");
require_once("logout.php");
require_once("addToCart.php");
?>
  <head>
    <meta charset="utf-8">
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://kit.fontawesome.com/60e43df7e9.js" crossorigin="anonymous"></script>
    <link href="./style.css" rel="stylesheet" type="text/css">
  </head>
  <body>
    <div id="mobile-navbar" class="slider show">
        <button><i class="fas fa-times" onclick="slideOut('mobile-navbar')"></i></button>
    </div>
    <div id="filters" class="slider show">
        <button><i class="fas fa-times" onclick="slideOut('filters')"></i></button>
        <h2> Categorie </h2>
                <ul>
                    <?php
                        $conn = new mysqli('localhost','root','','ecommerce');
                        $query = $conn->query("SELECT * FROM categoria");
                        if($query ->num_rows){
                            while ($row = $query->fetch_assoc()){
                                $id_cat = $row["id"];
                                $nome_cat = $row["nome"];
                                echo("
                                <li>
                                <div class=\"dropdown\">
                                    <span class=\"dropdown-click\"><form style=\"display:inline-block;\" action=\"\" method=\"post\"><button type=\"submit\" name=\"filterCat\" style=\"background: white;color: black;font-size: 16px;padding: 0;height: auto;\">$nome_cat</button><input type=\"hidden\" value=\"$id_cat\" name=\"cat\"></form><i class=\"fas fa-chevron-down\"
                                            style=\"float: right;\"></i></span>
                                ");
                                $query_sotcat = $conn->query("SELECT * FROM sottocategoria WHERE id_cat = $id_cat");
                                if($query_sotcat ->num_rows){
                                    while ($row = $query_sotcat->fetch_assoc()){
                                        $id_sotcat = $row["id"];
                                        $nome_sotcat = $row["nome"];
                                        echo("
                                        <div class=\"dropdown-content\">
                                        <ul class=\"noborder\">
                                            <li><form action=\"\" method=\"post\"><button type=\"submit\" name=\"filterSotCat\" style=\"background: white;color: black;font-size: 16px;padding: 0;height: auto;\">$nome_sotcat</button><input type=\"hidden\" value=\"$id_sotcat\" name=\"sotcat\"></form></li>
                                        </ul>
                                    </div>
                                        ");
                                    }
                                }
                                echo("
                                </div>
                                </li> 
                                ");

                            }
                        }
                        $conn -> close();   
                    ?>
                    <!--<li>
                        <div class="dropdown">
                            <span class="dropdown-click">Dropdown 1<i class="fas fa-chevron-down"
                                    style="float: right;"></i></span>
                            <div class="dropdown-content">
                                <ul class="noborder">
                                    <li>Dongle 1</li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="dropdown">
                            <span class="dropdown-click">Dropdown 2<i class="fas fa-chevron-down"
                                    style="float: right;"></i></span>
                            <div class="dropdown-content">
                                <ul class="noborder">
                                    <li>Dongle 2</li>
                                </ul>
                            </div>
                        </div>
                    </li>-->
                </ul>
            </div>
    </div>
    <div class="modal-container flexbox align-center justify-center" id="login-container">
        <div class="modal fade" id="accedi">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="close"
                            onclick="toggle('login-container')"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Accedi a Cristocaves</h4>
                    </div>
                    <div id="errorMessage"></div>
                    <div class="modal-body">
                        <form action="" method="post" onSubmit="return validateUser();">
                            <input type="email" placeholder="Email" class="bordered form-control" name="emailAcc" id="emailAccedi">
                            <input type="password" placeholder="Password" class="bordered form-control" name="passAcc" id="passwordAccedi">
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Accedi</button>
                            </div>                        
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="modal-container flexbox align-center justify-center" id="register-container">
        <div class="modal fade" id="accedi">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="close"
                            onclick="toggle('register-container')"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Registrati a Cristocaves</h4>
                    </div>
                    <div id="errorMessage2"></div>
                    <div class="modal-body">
                        <form action="" method="post">
                            <input type="text" placeholder="Nome" class="bordered" class="form-control" name="namereg" id="nomeregCli">
                            <input type="email" placeholder="Email" class="bordered" class="form-control" name="emailreg" id="emailregCli">
                            <input type="password" placeholder="Password" class="bordered" class="form-control" name="passwordreg" id="passwordregCli">
                            <input type="password" placeholder="Conferma password" class="bordered" class="form-control"  id="confPasswordregCli">
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Registrati</button>
                            </div>
                        </form>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <header id="header">
            <div class="container flexbox justify-between align-center wrap" id="main-h">
                <h1 class="logo"><a href="index.php" style="color: white;text-decoration: none;">Plantemall</a></h1>
                <div>
                    <div class="controls flexbox align-center wrap">
                        <form action="ricerca.php" method="GET">
                            <div class="row">
                                <i class="fas fa-search"></i>
                                <input type="search" name="q" placeholder="Cerca una piantina...">
                            </div>
                        </form>
                        <div class="icon">
                            <a href="cart.php" style="color:white;"><i class="fas fa-shopping-cart"></i></a>
                        </div>
                        <div class="icon">
                            <a href="ordini.php" style="color:white;"><i class="fas fa-user"></i></a>
                        </div>
                    </div>
                    <div class="flexbox" style="margin-top: 10px;">
                        <?php
                            if(empty($_SESSION["email"])){
                        ?>
                        <button style="border: 2px solid white;" onclick="toggle('login-container')"> Login </button>
                        <button style="border: 2px solid white; margin-left: 10px;"
                            onclick="toggle('register-container')"> Registrati </button>
                        <?php
                            }else{
                        ?>
                            <form action="" method="post">
                                <button name="logout" style="border: 2px solid white; margin-left: 10px;">Logout da <?php echo($_SESSION["email"]); ?></button>
                            </form>
                        <?php
                            }
                        ?>
                    </div>
                </div>
            </div>
        <div id="navbar" class="flexbox align-center">
            <nav>
                <a href="index.php">Home</a>
                <a href="log.php">Notifiche</a>
                <a href="ordini.php">Ordini</a>
                <?php 
                    if(!empty($_SESSION["admin"])){
                ?>
                <a>|</a>
                <a href="dashboard-admin.php">Aggiungi prodotti</a>
                <a href="log_ordini.php">Log ordini</a>
                <a href="utenti.php">Utenti</a>
                <?php
                    }
                ?>
            </nav>
        </div>
    </header>
    <div id="main">
      <div class="content">
        <div class="header">
          <h2>Acquisto Completato</h2>
        </div>
        <p style="width:100%;"><span class="fas fa-check"></span></p>
        <p>L'acquisto è stato eseguito con successo e procederemo ora alla spedizione!</p>
      </div>
                          </div>
                          <footer class ="footer">
        <div class ="footer-left"> 
            <h1>Plantemall</h1>
            <p>Siamo specializzati in vendita di svariati tipi di piante, il sito è stato creato per rendere possibile, anche durante la 
                pandemia di poter comprare le tue piante preferite comodamente da casa! La spedizione effettuata sarà sempre gratuita
                ed impiegherà al massimo 3 giorni per arrivare a casa tua, in modo da preservare al meglio l'integrità della pianta.
            </p>
            <div class ="socials">
                <a href="#"><i class="fa fa-facebook"></i></a>
                <a href="#"><i class="fa fa-twitter"></i></a>
                <a href="#"><i class="fa fa-instagram"></i></a>
            </div>
        </div>
        <ul class="footer-right">
            <li>
                <h1>Address</h1>
                <ul class ="box">
                    <li>Via dei Giardini, 12</li>
                    <li>Bassano del Grappa(VI)</li>
                    <li>Italia</li>
                </ul>
            </li>
            <li>
                <h1>Orari di apertura</h1>
                <ul class ="box">
                    <li>Martedì: 8.30-12.30 14.30-18.30</li>
                    <li>Mercoledì: 8.30-12.30 14.30-18.30</li>
                    <li>Giovedì: 8.30-12.30 14.30-18.30</li>
                    <li>Venerdì: 8.30-12.30 14.30-18.30</li>
                    <li>Sabato: 8.30-12.30 14.30-18.30</li>
                    <li>Domenica: 8.30-12.30 14.30-18.30</li>
                </ul>
            </li>
        </ul>
        <div class ="footer-bottom">
            <p>
                All Right reserved by davide.manieri@studio.unibo.it
            </p>
        </div>
    </footer>

  </body>
 
</html>