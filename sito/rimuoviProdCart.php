<?php

if(session_status()==PHP_SESSION_NONE){
  session_start();
}
$conn = new mysqli('localhost','root','','plant');

$idprodotto = -1;

$query = "DELETE FROM in_carrello WHERE IDprodotto = ? AND IDuser = ?";

/*<a class="btn border-danger text-danger font-weight-normal my-2 ml-2" href="removeProd.php?type=carrello&idprod=<?php echo $prod["IDprodotto"];?>" role="button">Rimuovi</a>*/
//stringa da mettere nel pulsante di rimozione 

if($_GET['type'] == "carrello"){
    if(isset($_GET["idprod"])){
        $idprodotto = $_GET["idprod"];
    }
    $st=$conn->stmt_init();     //oppure mi conviene fare in questo modo?
    if($st->prepare($query)){
      $st->bind_param('ss',$idprod,$_SESSION["IDuser"]); 
      $st->execute(); 
    }
    //$query = "INSERT INTO in_carrello(IDprodotto, IDuser, quantita) VALUES (?, ?, 1)";
    //$dbh->insertInCart($idprodotto, $_SESSION["IDuser"]);
    header('Location: ' . $_SERVER['HTTP_REFERER']);
}

?>