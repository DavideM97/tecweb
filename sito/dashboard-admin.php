<DOCTYPE  !html>
<html lang="it">
<head>
    <meta charset="utf-8">
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://kit.fontawesome.com/60e43df7e9.js" crossorigin="anonymous"></script>
    <link href="./style.css" rel="stylesheet" type="text/css">
</head>
<?php
if(session_status()==PHP_SESSION_NONE){
    session_start();
  }
    if(empty($_SESSION["admin"])){
        header("location:/SitoMagno/sito/index.php");
    }
    require_once("addNotifica.php");
    require_once("registrazione.php");
    require_once("accesso.php");
    require_once("logout.php");
    require_once("aggiungiProdotto.php");
?>
<body>
<header id="header">
            <div class="container flexbox justify-between align-center wrap" id="main-h">
                <h1 class="logo"><a href="index.php" style="color: white;text-decoration: none;">Plantemall</a></h1>
                <div>
                    <div class="controls flexbox align-center wrap">
                        <form action="ricerca.php" method="GET">
                            <div class="row">
                                <i class="fas fa-search"></i>
                                <input type="search" name="q" placeholder="Cerca una piantina...">
                            </div>
                        </form>
                        <div class="icon">
                            <a href="cart.php" style="color:white;"><i class="fas fa-shopping-cart"></i></a>
                        </div>
                        <div class="icon">
                            <a href="ordini.php" style="color:white;"><i class="fas fa-user"></i></a>
                        </div>
                    </div>
                    <div class="flexbox" style="margin-top: 10px;">
                        <?php
                            if(empty($_SESSION["email"])){
                        ?>
                        <button style="border: 2px solid white;" onclick="toggle('login-container')"> Login </button>
                        <button style="border: 2px solid white; margin-left: 10px;"
                            onclick="toggle('register-container')"> Registrati </button>
                        <?php
                            }else{
                        ?>
                            <form action="" method="post">
                                <button name="logout" style="border: 2px solid white; margin-left: 10px;">Logout da <?php echo($_SESSION["email"]); ?></button>
                            </form>
                        <?php
                            }
                        ?>
                    </div>
                </div>
            </div>
        <div id="navbar" class="flexbox align-center">
            <nav>
                <a href="index.php">Home</a>
                <a href="log.php">Notifiche</a>
                <a href="ordini.php">Ordini</a>
                <?php 
                    if(!empty($_SESSION["admin"])){
                ?>
                <a>|</a>
                <a href="dashboard-admin.php">Aggiungi prodotti</a>
                <a href="log_ordini.php">Log ordini</a>
                <a href="utenti.php">Utenti</a>
                <?php
                    }
                ?>
            </nav>
        </div>
    </header>
<div id="main">
    <h1 id="main-title"> Tutte le piante </h1>
    <button class="show" style="margin: 0 auto; margin-bottom: 40px;" onclick="slideIn('filters')">Filtri</button>
    <div class="flexbox align-top" id="main-grid">
        <div class="sidebar" id="main-sidebar">
            <h2> Aggiungi prodotto </h2>
            <ul>
            <form style="width: 100%;" action = "" method = "post">
                <div class="modal-body">
                        <input type="text" placeholder="Filename" class="bordered" name="filename">
                        <input type="text" placeholder="Nome piantina" class="bordered" name="nome">
                        <input type="text" placeholder="ID Categoria" class="bordered" name="cat">
                        <input type="text" placeholder="ID Sotto Categoria" class="bordered" name="sotcat">
                        <textarea placeholder="Descrizione" class="bordered" style="height: 200px;" name="descr"></textarea>
                        <input type="text" placeholder="Immagine" class="bordered" name="img">
                        <input type="text" placeholder="Prezzo" class="bordered" name="prezzo">
                    
                </div>
                <div class="modal-footer">
                    <button type="submit" name="createProd" class="btn btn-primary align-center">Aggiungi piantina</button>
                </div>
                </form>
            </ul>
        </div>
        <div class="main-view flexbox wrap" id="main-flexbox">
        <?php
                    $conn = new mysqli('localhost','root','','ecommerce');
                    $query = $conn->query("SELECT * FROM prodotto ORDER BY nome");
                    if($query ->num_rows){
                        while ($row = $query->fetch_assoc()){
                            $id_prod = $row["id"];
                            $nome = $row["nome"];
                            $descr = $row["descrizione"];
                            $img = $row["immagine"] . ".jpg";
                            $prezzo = $row["prezzo"];
                            $filename = $row["filename"];
                            echo("
                            <div class=\"item\">
                                <a href=\"$filename\" target=\"_blank\">
                                    <img src=\"images/$img\">
                                </a>
                                <p>$nome</p>
                                <h2>$prezzo €</h2>
                            </div>
                            ");
                        }
                    }
                ?>
        </div>
    </div>
</div>
<footer class ="footer">
        <div class ="footer-left"> 
            <h1>Plantemall</h1>
            <p>Siamo specializzati in vendita di svariati tipi di piante, il sito è stato creato per rendere possibile, anche durante la 
                pandemia di poter comprare le tue piante preferite comodamente da casa! La spedizione effettuata sarà sempre gratuita
                ed impiegherà al massimo 3 giorni per arrivare a casa tua, in modo da preservare al meglio l'integrità della pianta.
            </p>
            <div class ="socials">
                <a href="#"><i class="fa fa-facebook"></i></a>
                <a href="#"><i class="fa fa-twitter"></i></a>
                <a href="#"><i class="fa fa-instagram"></i></a>
            </div>
        </div>
        <ul class="footer-right">
            <li>
                <h1>Address</h1>
                <ul class ="box">
                    <li>Via dei Giardini, 12</li>
                    <li>Bassano del Grappa(VI)</li>
                    <li>Italia</li>
                </ul>
            </li>
            <li>
                <h1>Orari di apertura</h1>
                <ul class ="box">
                    <li>Martedì: 8.30-12.30 14.30-18.30</li>
                    <li>Mercoledì: 8.30-12.30 14.30-18.30</li>
                    <li>Giovedì: 8.30-12.30 14.30-18.30</li>
                    <li>Venerdì: 8.30-12.30 14.30-18.30</li>
                    <li>Sabato: 8.30-12.30 14.30-18.30</li>
                    <li>Domenica: 8.30-12.30 14.30-18.30</li>
                </ul>
            </li>
        </ul>
        <div class ="footer-bottom">
            <p>
                All Right reserved by davide.manieri@studio.unibo.it
            </p>
        </div>
    </footer>
</body>
</html>