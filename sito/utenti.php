<DOCTYPE  !html>
<html>
<head>
    <meta charset="utf-8">
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://kit.fontawesome.com/60e43df7e9.js" crossorigin="anonymous"></script>
    <link href="./style.css" rel="stylesheet" type="text/css">
</head>
<?php
if(session_status()==PHP_SESSION_NONE){
    session_start();
  }
    if(empty($_SESSION["admin"])){
        header("location:/SitoMagno/sito/index.php");
    }
    require_once("addNotifica.php");
    require_once("registrazione.php");
    require_once("accesso.php");
    require_once("logout.php");
?>
<body>
<header id="header">
            <div class="container flexbox justify-between align-center wrap" id="main-h">
                <h1 class="logo"><a href="index.php" style="color: white;text-decoration: none;">Plantemall</a></h1>
                <div>
                    <div class="controls flexbox align-center wrap">
                        <form action="ricerca.php" method="GET">
                            <div class="row">
                                <i class="fas fa-search"></i>
                                <input type="search" name="q" placeholder="Cerca una piantina...">
                            </div>
                        </form>
                        <div class="icon">
                            <a href="cart.php" style="color:white;"><i class="fas fa-shopping-cart"></i></a>
                        </div>
                        <div class="icon">
                            <a href="ordini.php" style="color:white;"><i class="fas fa-user"></i></a>
                        </div>
                    </div>
                    <div class="flexbox" style="margin-top: 10px;">
                        <?php
                            if(empty($_SESSION["email"])){
                        ?>
                        <button style="border: 2px solid white;" onclick="toggle('login-container')"> Login </button>
                        <button style="border: 2px solid white; margin-left: 10px;"
                            onclick="toggle('register-container')"> Registrati </button>
                        <?php
                            }else{
                        ?>
                            <form action="" method="post">
                                <button name="logout" style="border: 2px solid white; margin-left: 10px;">Logout da <?php echo($_SESSION["email"]); ?></button>
                            </form>
                        <?php
                            }
                        ?>
                    </div>
                </div>
            </div>
        <div id="navbar" class="flexbox align-center">
            <nav>
                <a href="index.php">Home</a>
                <a href="log.php">Notifiche</a>
                <a href="ordini.php">Ordini</a>
                <?php 
                    if(!empty($_SESSION["admin"])){
                ?>
                <a>|</a>
                <a href="dashboard-admin.php">Aggiungi prodotti</a>
                <a href="log_ordini.php">Log ordini</a>
                <a href="utenti.php">Utenti</a>
                <?php
                    }
                ?>
            </nav>
        </div>
    </header>
<div id="main">
    <h1 id="main-title"> Tutti gli utenti </h1>
    <div class="table-container">
	<table>
		<thead>
			<tr>
                <th>Email</th>
                <th>Nome</th>
                <th>Data Registrazione</th>
			</tr>
		</thead>
		<tbody>
			<?php
                $conn = new mysqli('localhost','root','','ecommerce');
                $query = $conn->query("SELECT * FROM utente ORDER BY data_registrazione");
                if($query ->num_rows){
                    while ($row = $query->fetch_assoc()){
                        $email = $row["email"];
                        $nome = $row["nome"];
                        $data = $row["data_registrazione"];
                        echo("
                            <tr>
                                <td>$email</td>
                                <td>$nome</td>
                                <td>$data</td>
                            </tr>
                        ");
                    }
                }
            ?>
		</tbody>
	</table>
</div>
</div>
<footer class ="footer">
        <div class ="footer-left"> 
            <h1>Plantemall</h1>
            <p>Siamo specializzati in vendita di svariati tipi di piante, il sito è stato creato per rendere possibile, anche durante la 
                pandemia di poter comprare le tue piante preferite comodamente da casa! La spedizione effettuata sarà sempre gratuita
                ed impiegherà al massimo 3 giorni per arrivare a casa tua, in modo da preservare al meglio l'integrità della pianta.
            </p>
            <div class ="socials">
                <a href="#"><i class="fa fa-facebook"></i></a>
                <a href="#"><i class="fa fa-twitter"></i></a>
                <a href="#"><i class="fa fa-instagram"></i></a>
            </div>
        </div>
        <ul class="footer-right">
            <li>
                <h1>Address</h1>
                <ul class ="box">
                    <li>Via dei Giardini, 12</li>
                    <li>Bassano del Grappa(VI)</li>
                    <li>Italia</li>
                </ul>
            </li>
            <li>
                <h1>Orari di apertura</h1>
                <ul class ="box">
                    <li>Martedì: 8.30-12.30 14.30-18.30</li>
                    <li>Mercoledì: 8.30-12.30 14.30-18.30</li>
                    <li>Giovedì: 8.30-12.30 14.30-18.30</li>
                    <li>Venerdì: 8.30-12.30 14.30-18.30</li>
                    <li>Sabato: 8.30-12.30 14.30-18.30</li>
                    <li>Domenica: 8.30-12.30 14.30-18.30</li>
                </ul>
            </li>
        </ul>
        <div class ="footer-bottom">
            <p>
                All Right reserved by davide.manieri@studio.unibo.it
            </p>
        </div>
    </footer>
</body>
</html>