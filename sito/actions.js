function toggle(id){
    
    const login_modal = document.getElementById(id);
    if(login_modal.classList.contains('toggled')){
        login_modal.style.opacity = "0";
        login_modal.style.pointerEvents = "none";
        login_modal.classList.remove('toggled');
    }else{
        login_modal.style.opacity = "1";
        login_modal.style.pointerEvents = "all";
        login_modal.classList.add('toggled')
    }

}
function slideIn(id){

    const el = document.getElementById(id);
    el.style.left = "0";

}
function slideOut(id){

    const el = document.getElementById(id);
    el.style.left = "-100vw";

}