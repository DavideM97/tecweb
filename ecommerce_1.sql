-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Giu 26, 2021 alle 12:30
-- Versione del server: 5.7.17
-- Versione PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ecommerce`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `cart`
--

CREATE TABLE `cart` (
  `id` int(11) NOT NULL,
  `id_prod` int(11) NOT NULL,
  `mail_utente` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `quantita` int(11) NOT NULL,
  `id_ordine` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `cart`
--

INSERT INTO `cart` (`id`, `id_prod`, `mail_utente`, `quantita`, `id_ordine`) VALUES
(9, 1, 'bho@bho.com', 2, 6),
(7, 1, 'bho@bho.com', 1, 5),
(10, 3, 'bho@bho.com', 1, 6),
(11, 2, 'bho@bho.com', 1, 7),
(12, 1, 'bho@bho.com', 2, NULL);

-- --------------------------------------------------------

--
-- Struttura della tabella `categoria`
--

CREATE TABLE `categoria` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `categoria`
--

INSERT INTO `categoria` (`id`, `nome`) VALUES
(1, 'Alberi da Frutto'),
(2, 'Conifere'),
(3, 'Latifoglie'),
(4, 'Piante Grasse');

-- --------------------------------------------------------

--
-- Struttura della tabella `notifica`
--

CREATE TABLE `notifica` (
  `id` int(11) NOT NULL,
  `mail_utente` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `tipologia` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `data_inserimento` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `notifica`
--

INSERT INTO `notifica` (`id`, `mail_utente`, `tipologia`, `data_inserimento`) VALUES
(1, 'bho@bho.com', 'login', '2021-06-21 15:54:19'),
(2, 'bho@bho.com', 'aggiunto_carrello', '2021-06-21 15:54:32'),
(3, 'bho@bho.com', 'aggiunto_carrello', '2021-06-21 15:54:45'),
(4, 'bho@bho.com', 'login', '2021-06-24 15:00:01'),
(5, 'bho@bho.com', 'login', '2021-06-24 15:00:15'),
(6, 'bho@bho.com', 'login', '2021-06-24 15:00:59'),
(7, 'bho@bho.com', 'checkout', '2021-06-24 16:42:46'),
(8, 'bho@bho.com', 'login', '2021-06-26 11:06:52'),
(9, 'bho@bho.com', 'login', '2021-06-26 11:07:00'),
(10, 'bho@bho.com', 'aggiunto_carrello', '2021-06-26 11:07:12'),
(11, 'bho@bho.com', 'checkout', '2021-06-26 11:07:34'),
(12, 'bho@bho.com', 'logout', '2021-06-26 11:19:58'),
(13, 'bho@bho.com', 'login', '2021-06-26 11:20:40'),
(14, 'bho@bho.com', 'prodotto_creato', '2021-06-26 11:25:31'),
(15, 'bho@bho.com', 'prodotto_creato', '2021-06-26 11:28:21'),
(16, 'bho@bho.com', 'prodotto_creato', '2021-06-26 11:30:26'),
(17, 'bho@bho.com', 'prodotto_creato', '2021-06-26 11:32:37'),
(18, 'bho@bho.com', 'prodotto_creato', '2021-06-26 11:35:04'),
(19, 'bho@bho.com', 'prodotto_creato', '2021-06-26 11:38:50'),
(20, 'bho@bho.com', 'prodotto_creato', '2021-06-26 11:40:42'),
(21, 'bho@bho.com', 'prodotto_creato', '2021-06-26 11:42:23'),
(22, 'bho@bho.com', 'prodotto_creato', '2021-06-26 11:44:56'),
(23, 'bho@bho.com', 'prodotto_creato', '2021-06-26 11:47:41'),
(24, 'bho@bho.com', 'login', '2021-06-26 11:49:29'),
(25, 'bho@bho.com', 'aggiunto_carrello', '2021-06-26 12:03:12'),
(26, 'bho@bho.com', 'aggiunto_carrello', '2021-06-26 12:03:22');

-- --------------------------------------------------------

--
-- Struttura della tabella `ordine`
--

CREATE TABLE `ordine` (
  `id` int(11) NOT NULL,
  `mail_utente` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `data_ordine` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `ordine`
--

INSERT INTO `ordine` (`id`, `mail_utente`, `data_ordine`) VALUES
(6, 'bho@bho.com', '2021-06-24 00:00:00'),
(5, 'bho@bho.com', '2021-06-23 00:00:00'),
(7, 'bho@bho.com', '2021-06-26 11:07:32');

-- --------------------------------------------------------

--
-- Struttura della tabella `prodotto`
--

CREATE TABLE `prodotto` (
  `id` int(11) NOT NULL,
  `filename` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `nome` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `descrizione` text CHARACTER SET utf8mb4 NOT NULL,
  `immagine` varchar(255) NOT NULL,
  `prezzo` int(11) NOT NULL,
  `id_cat` int(11) NOT NULL,
  `id_sottocat` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `prodotto`
--

INSERT INTO `prodotto` (`id`, `filename`, `nome`, `descrizione`, `immagine`, `prezzo`, `id_cat`, `id_sottocat`) VALUES
(1, 'Albicocca_Goldrich.php', 'Albicocca Goldrich', 'Albero vigoroso, di media produttivita\'. \r\nIl frutto e\' di calibro grosso, con buccia di colore arancio intenso; \r\nmolto dolce e gustoso, soprattutto a maturazione avanzata.\r\nE\' conosciuta anche con il nome \"Sungiant\". \r\nE\' una delle varietà più coltivate in quanto i frutti riescono a tenere \r\nmolto bene sulla pianta, così da consentire un periodo di raccolta \r\npiù lungo. \r\nLa raccolta avviene, di solito, nella seconda metà del mese di giugno. \r\nI frutti sono utilizzati per il consumo fresco.', 'Albicocca_Goldrich', 1300, 1, 1),
(2, 'Albicocca_Reale_D_Imola.php', 'Albicocca Reale d\'Imola', 'E\' una delle varieta\' più conosciute, la pianta e\' di buon vigore\r\ncon abbondanti produzioni, i frutti hanno forma allungata e sono \r\ngrossi con buccia gialla a sfumature rossastre, la polpa e giallo\r\nchiaro molto dolce e profumata. \r\nMatura da meta\' giugno ai primi di luglio.\r\nE\' una pianta che si distingue per una produttività non costante; \r\nessa si dimostra più abbondante al Nord Italia, \r\nmentre e\' media al centro e al sud. \r\nSe la pianta non viene sottoposta a diradamento la \r\nproduttività va a scalare e dura all\'incirca tre settimane.', 'Albicocca_Reale', 1000, 1, 1),
(3, 'Albicocco_Ivonne_Liverani.php', 'Albicocco Ivonne Liverani', 'Albero di media vigoria a portamento intermedio. \r\nE’ caratterizzato da un elevata resistenza alle malattie e \r\nalle gelate primaverili. \r\nLa sua produttività risulta elevata e costante infatti la pianta \r\nfruttifica sui dardi e rami misti.\r\nFiorisce precocemente e i frutti sono medi a forma ovata, \r\ncon buccia color giallo-arancio e polpa aranciata. \r\nSapore dolce e di breve conservazione.\r\nMaturazione 20-30 giugno.', 'Albicocco_Ivonne_Liverani', 1250, 1, 1),
(9, 'Ciliegia_Bigarreau_Burlat.php', 'Ciliegia Bigarreau Burlat', 'Varieta\' di origine francese, con albero dal portamento assurgente \r\nespanso, caratterizzato da vigoria e produttività media.\r\nIl frutto e\' di pezzatura buona, di forma a cuore, \r\ncon peduncolo di media lunghezza; \r\nla buccia e\' di colore rosso cupo e polpa rossa di consistenza \r\nmedia elevata, buona succosita\'; sapore dolce e conservazione breve.\r\nMaturazione: 20-30 maggio.', 'Ciliegia_Bigarreau_Burlat', 1350, 1, 2),
(15, 'Ciliegia_Bigarreau_Moreau.php', 'Ciliegia Bigarreau Moreau', 'Cultivar francese molto presente in Emilia Romagna; \r\nse ne conoscono due tipi, quella disponibile è \r\nil tipo B che produce frutti grandi cuoriformi \r\ncon buccia rosso cupo e polpa rossa scura.\r\nIl peduncolo di media lunghezza e il nocciolo è grosso. \r\nFiorisce precocemente ed è autoincompatibile. \r\nViene impollinata da Mora di Vignola, Durone nero I, \r\nBigarreau Burlat. Cultivar produttiva con una precoce \r\nmessa a frutto di sapore dolce e conservazione breve.\r\nMaturazione: 20-30 maggio.', 'Ciliegia_Bigarreau_Moreau', 2000, 1, 2),
(16, 'Ciliegia_Durona_di_Vignola.php', 'Ciliegia Durona di Vignola', 'Famosa varietà conosciuta per il suo grosso calibro di \r\nun colore rosso scuro quasi nero; \r\ndeve il suo nome alla polpa consistente che le ha permesso \r\ndi essere coltivata anche in grande scala perché adatta ai trasporti.\r\nE’ una varietà vigorosa produttiva, per l’impollinazione sono \r\nadatte Bigarreau Moreau, Bigarreau Burlat.\r\nNel tempo ne sono stati selezionati diversi cloni che \r\ndifferiscono principalmente per l’epoca di maturazione, \r\nquello che noi proponiamo è a maturazione intermedia \r\ne cioè a metà giugno.', 'Ciliegia_Durona_di_Vignola', 1860, 1, 2);

-- --------------------------------------------------------

--
-- Struttura della tabella `sottocategoria`
--

CREATE TABLE `sottocategoria` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `id_cat` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `sottocategoria`
--

INSERT INTO `sottocategoria` (`id`, `nome`, `id_cat`) VALUES
(1, 'Albicocchi', 1),
(2, 'Ciliegi', 1),
(3, 'Peschi', 1),
(4, 'Abeti', 2),
(5, 'Cedri', 2),
(6, 'Cipressi', 2),
(7, 'Acacie', 3),
(8, 'Aceri', 3),
(9, 'Cactacee', 4),
(10, 'Succulente', 4);

-- --------------------------------------------------------

--
-- Struttura della tabella `utente`
--

CREATE TABLE `utente` (
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `nome` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `password` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `data_registrazione` datetime NOT NULL,
  `admin` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `utente`
--

INSERT INTO `utente` (`email`, `nome`, `password`, `data_registrazione`, `admin`) VALUES
('bho@bho.com', 'Dennis Cavina', 'MTIzc3RlbGxh', '2021-06-16 10:22:13', 1);

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `notifica`
--
ALTER TABLE `notifica`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `ordine`
--
ALTER TABLE `ordine`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `prodotto`
--
ALTER TABLE `prodotto`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `sottocategoria`
--
ALTER TABLE `sottocategoria`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `utente`
--
ALTER TABLE `utente`
  ADD PRIMARY KEY (`email`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT per la tabella `categoria`
--
ALTER TABLE `categoria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT per la tabella `notifica`
--
ALTER TABLE `notifica`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT per la tabella `ordine`
--
ALTER TABLE `ordine`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT per la tabella `prodotto`
--
ALTER TABLE `prodotto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT per la tabella `sottocategoria`
--
ALTER TABLE `sottocategoria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
