# Plantemall #


Il progetto da me svolto consiste in un sito di e-commerce che si occupa della vendita di piante e alberi da frutto

### Fase di progettazione ###
Inizialmente sono stati eseguiti i mockup per avere un'idea generale della direzione da prendere in fase realizzativa. Questi potrebbero aver subito modifiche durante la fase di sviluppo

### Fase di sviluppo ###
Basandosi sui mockup precedentemente realizzata e' stata fatta l'implementazioni delle pagine utilizzando HTML e CSS. Dopo aver verificato la responsiveness del sito e la sua accessibilita' e' stato creato un database sulla quale
poggia PHP.

### Controllo di qualità ###
Al termine dello sviluppo il sito e' stato testato per verificare eventuali mancanze.


